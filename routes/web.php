<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Events\NewNotif;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create', function (){
    return view('create');
})->name('create');

Route::get('gen_game', 'GameController@create')->name('game.create');

Route::post('gen_game', 'GameController@store')->name('game.store');


Route::get('game/{_id}',['uses' => 'GameController@view'])->name('game');

Route::get('notif/{_id}/{sender}',['uses' => 'UserController@remove_notif'])->name('notif');

Route::post('add_hand', 'GameController@generateParams')->name('hand.post');

Route::get('autocomplete', 'SearchController@search');

Route::get('input', 'GameController@getInput');

Route::get('logout', 'Auth\LoginController@logout', function () {
    return abort(404);
});
Route::get('/event',function () {
    event(new NewNotif("yo"));
});
