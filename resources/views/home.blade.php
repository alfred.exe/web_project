@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a> <a class="btn-floating btn-large waves-effect waves-light red" href={{route('create')}}><i
                            class="material-icons">add</i></a>
                </a>
                @foreach ($data as $game)
                    @component('components.game', ['game'=>$game])

                    @endcomponent
                @endforeach
            </div>

        </div>
    </div>
    </div>
@endsection
