<div class="game-container">

        <a href={{route('game', ['_id'=>$game['_id']])}}>
                <table class="centered">
                    <thead>
                    <tr>
                        <th>{{$game['p1']}}</th>
                        <th>{{$game['p2']}}</th>
                        <th>{{$game['p3']}}</th>
                        @isset($game['p4'])
                            <th>
                                {{$game['p4']}}
                            </th>
                        @endisset
                        @isset($game['p5'])
                            <th>
                                {{$game['p5']}}
                            </th>
                        @endisset
                    </tr>
                    </thead>
                    <tbody>

                    <tr>@isset($game["total"])
                            @foreach($game["total"] as $total)
                                <td>
                                    {{$total}}
                                </td>
                            @endforeach
                        @endisset
                    </tr>
                    </tbody>

                </table>
        </a>
</div>

