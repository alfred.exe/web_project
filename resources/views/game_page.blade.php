@extends('layouts.app')

@section('content')
    <div class="container">
        @if($game['status'] == 0)
            @foreach($game["wait"] as $waited)
                <div>
                    On attends ce bon vieux {{$waited}}
                </div>
            @endforeach
        @else


            <div class="game_page_content">
                <table class="centered" id="score_table">
                    <thead>
                    <tr>

                        <th>{{$game['p1']}}</th>
                        <th>{{$game['p2']}}</th>
                        <th>{{$game['p3']}}</th>
                        @isset($game['p4'])
                            <th>
                                {{$game['p4']}}
                            </th>
                        @endisset
                        @isset($game['p5'])
                            <th>
                                {{$game['p5']}}
                            </th>
                    </tr>

                    @endisset

                    </thead>

                    <tbody id="score_table_body">
                    @isset($game['hand'])
                        @foreach ($game['hand'] as $hand)
                            <tr class="tablerow" array-index={{$loop->index}}>
                                <td>
                                    {{$hand[0]}}
                                </td>
                                <td>
                                    {{$hand[1]}}
                                </td>
                                <td>
                                    {{$hand[2]}}
                                </td>
                                @isset($hand[3])
                                    <td>
                                        {{$hand[3]}}
                                    </td>
                                @endisset
                                @isset($hand[4])
                                    <td>
                                        {{$hand[4]}}
                                    </td>
                                @endisset
                            </tr>
                        @endforeach
                        <tr class="tablerow">
                            @isset($game['total'])
                                @foreach($game['total'] as $total)
                                    <td>{{$total}}</td>
                                @endforeach
                            @endisset
                        </tr>

                        <tr class="tablerow">
                            <td class="tablerow" colspan="3" id="add">+</td>
                        </tr>
                    @endisset
                    </tbody>
                </table>
            </div>
            <div>
                <div class="row justify-content-center">
                    <div class="row">
                        <form method="post" action={{route("hand.post", ['_id' => $game['_id']])}} id="hand"
                              onsubmit="return checkForm()">
                            {{ csrf_field() }}
                            {{--//TODO load css class dinamically --}}

                            <div class="input-hand">

                                <div class="radio-input">
                                    <label class="custom_radio trio">
                                        {{$game['p1']}}
                                        <input id="preneur0" type="checkbox" class="radio" value="0" name="preneur">
                                        <span class="lever"></span>
                                    </label>
                                    <label class="custom_radio trio">
                                        {{$game['p2']}}
                                        <input id="preneur1" type="checkbox" class="radio" value="1" name="preneur">
                                        <span class="lever"></span>

                                    </label>
                                    <label class="custom_radio trio">
                                        {{$game['p3']}}
                                        <input id="preneur2" type="checkbox" class="radio" value="2" name="preneur">
                                        <span class="lever"></span>
                                    </label>
                                    @if ($game['p4'] !== null)
                                        <label class="custom_radio">
                                            $game['p4']
                                            <input id="preneur3" type="checkbox" class="radio" value="1"
                                                   name="preneur">
                                            <span class="lever"></span>
                                        </label>
                                    @endif
                                    @if ($game['p5'] !== null)
                                        <label class="custom_radio">
                                            $game['p5']
                                            <input id="preneur4" type="checkbox" class="radio" value="1"
                                                   name="preneur">
                                            <span class="lever"></span>
                                        </label>
                                    @endif
                                </div>
                                <div class="radio-input">

                                    {{--TODO add label and maybe change buttons--}}
                                    <label class="custom_radio">
                                        Petite
                                        <input id="prise0" type="checkbox" class="radio" value="0" name="prise">
                                        <span class="lever"></span>
                                    </label>
                                    <label class="custom_radio">
                                        Garde
                                        <input id="prise1" type="checkbox" class="radio" value="1" name="prise">
                                        <span class="lever"></span>
                                    </label>
                                    <label class="custom_radio">
                                        Garde Contre
                                        <input id="prise2" type="checkbox" class="radio" value="2" name="prise">
                                        <span class="lever"></span>
                                    </label>
                                    <label class="custom_radio">
                                        Garde Sans
                                        <input id="prise3" type="checkbox" class="radio" value="3" name="prise">
                                        <span class="lever"></span>
                                    </label>
                                </div>

                                <div class="radio-input quatuor">
                                    <label class="custom_radio">
                                        0
                                        <input id="bouts0" type="checkbox" class="radio" value="0" name="bouts">
                                        <span class="lever"></span>
                                    </label>
                                    <label class="custom_radio">
                                        1
                                        <input id="bouts1" type="checkbox" class="radio" value="1" name="bouts">
                                        <span class="lever"></span>
                                    </label>
                                    <label class="custom_radio">
                                        2
                                        <input id="bouts2" type="checkbox" class="radio" value="2" name="bouts">
                                        <span class="lever"></span>

                                    </label>
                                    <label class="custom_radio">
                                        3
                                        <input id="bouts3" type="checkbox" class="radio" value="3" name="bouts">
                                        <span class="lever"></span>
                                    </label>
                                </div>
                            </div>
                            <p class="range-field">
                                <input type="range" name="points" id="points_slider" min="0" value="46" max="91"/>
                            </p>
                            <div id="contrat">
                                <div id="attack" class="att_def">
                                    <button type="button" class="btn" id="attack_plus">+</button>
                                    <p id="attack_points">46</p>
                                    <button type="button" class="btn" id="attack_minus">-</button>
                                </div>
                                <div id="points_needed">56</div>
                                <div id="defense" class="att_def">
                                    <button type="button" class="btn" id="defense_plus">+</button>
                                    <p id="defense_points">45</p>
                                    <button type="button" class="btn" id="defense_minus">-</button>
                                </div>
                            </div>
                            <div id="indicator">Contrat</div>

                        </form>
                    </div>
                    <div id="error"></div>
                    <div id="submit_container">

                    </div>
                    <button type="submit" form="hand" class="" value="Submit">Ajouter</button>

                </div>

            </div>
            <div id="timeline" style="width:100%; height:400px;"></div>
            <div id="contrats" style="width:100%; height:400px;"></div>
            <div id="bouts" style="width:100%; height:400px;"></div>
            <div id="preneurs" style="width:100%; height:400px;"></div>
            <div id="win_hand" style="width:100%; height:400px;"></div>
            <div id="best_hand" style="width:100%; height:400px;">
                <table>
                    <thead>
                    <tr>

                        <th>{{$game['p1']}}</th>
                        <th>{{$game['p2']}}</th>
                        <th>{{$game['p3']}}</th>
                        @isset($game['p4'])
                            <th>
                                {{$game['p4']}}
                            </th>
                        @endisset
                        @isset($game['p5'])
                            <th>
                                {{$game['p5']}}
                            </th>
                        @endisset
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @isset($game->hand[$stats["index_max_hand"]])

                        @foreach($game->hand[$stats["index_max_hand"]] as $point)
                            <td>{{$point}}</td>
                        @endforeach
                        @endisset
                    </tr>
                    </tbody>
                </table>

            </div>
        @endif

    </div>
@endsection
<script src="https://code.highcharts.com/highcharts.js"></script>
<script>
    // the selector will match all input ctrols of type :checkbox
    // and attach a click event handler
    window.onload = function () {
        $("input:checkbox").on('click', function () {
            // in the handler, 'this' refers to the box clicked on
            const $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                const group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
                $(group).parent().removeClass("input_checked");
                $box.parent().addClass("input_checked");

            } else {
                $box.prop("checked", false);
                $box.parent().removeClass("input_checked");

            }
        });
        var points_needed = 56;
        $("input:checkbox[name='bouts']").on("click change", (e) => {
            points_needed = pointsNeeded(e);
        })
        const slider = $("#points_slider");
        slider.on('change', () => {
            calcPoints(points_needed, slider.val());

        });
        $("#attack_plus").on("click", () => {
            let value = parseInt(slider.val()) + 1;
            slider.val(value);
            calcPoints(points_needed, value);
        });
        $("#attack_minus").on("click", () => {
            let value = parseInt(slider.val()) - 1;
            slider.val(value);
            calcPoints(points_needed, value);
        });
        $("#defense_plus").on("click", () => {
            let value = parseInt(slider.val()) - 1;
            slider.val(value);
            calcPoints(points_needed, value);
        });
        $("#defense_minus").on("click", () => {
            let value = parseInt(slider.val()) + 1;
            slider.val(value);
            calcPoints(points_needed, value);
        })
        $(".tablerow").not(":eq(-2), :eq(-1)").on("click", function (e) {
            const index = $(this).attr("array-index");
            $("#selected").attr("id", "");
            const route = "{{ url('input') }}";

            $.get(route, {index: index, game: "{{$game["_id"]}}"}, (data) => {
                $("#points_slider").val(data[0]);
                if (!$("#bouts" + data[1].toString()).is(":checked")) {
                    $("#bouts" + data[1].toString()).click();
                }
                if (!$("#prise" + data[2].toString()).is(":checked")) {
                    $("#prise" + data[2].toString()).click();
                }
                if (data[3]) {
                    if (!$("#preneur" + data[3].toString()).is(":checked")) {
                        $("#preneur" + data[3].toString()).click();
                    }
                }
                console.log(pointsNeeded(data[1]));
                console.log(data[0]);
                calcPoints(pointsNeeded(data[1]), data[0])
            });
            $("#hand").append('<input type="hidden" id="index" name="index" value=' + index + ' /> ');
            $(this).attr("id", "selected");
        });
        $('tbody > tr').filter(":eq(-2), :eq(-1)").click(function (e) {
            $("#selected").attr("id", "");
            $(this).attr("id", "selected");
            $("#points_slider").val(45);
            $('input[name="bouts"]').each(function (obj, i) {
                if ($(this).is(":checked")) {
                    $(this).click();
                }
            });
            $('input[name="preneur"]').each(function (obj, i) {
                if ($(this).is(":checked")) {
                    $(this).click();
                }
            });
            $('input[name="prise"]').each(function (obj, i) {
                if ($(this).is(":checked")) {
                    $(this).click();
                }
            });
            $("#index").remove();
        });
        const stats = ({!! json_encode($stats) !!});
        const game = ({!! json_encode($game) !!});
        console.log(stats);
        /*data: [{
            name: "Petite",
            y: stats.prise["0"] ? stats.prise["0"].avg : 0,
        }, {
            name: "Garde",
            y: stats.prise["1"] ? stats.prise["1"].avg : 0,
        }, {
            name: "Garde Sans",
            y: stats.prise["2"] ? stats.prise["2"].avg : 0,syntax error, unexpected end of file
        }, {
            name: "Garde Contre",
            y: stats.prise["3"] ? stats.prise["3"].avg : 0,
        }]*/
        const timeline = Highcharts.chart('timeline', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'Timeline'
            },
            yAxis: {
                title: {
                    text: 'Points'
                }
            },
            series: [{
                name: game.p1,
                data: stats.line["0"],
            }, {
                name: game.p2,
                data: stats.line["1"],
            }, {
                name: game.p3,
                data: stats.line["2"],
            },]
        });
        const contrats = Highcharts.chart('contrats', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Repartition contrats'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: "Petite",
                    y: stats.prise["0"] ? stats.prise["0"].avg : 0,
                }, {
                    name: "Garde",
                    y: stats.prise["1"] ? stats.prise["1"].avg : 0,
                }, {
                    name: "Garde Sans",
                    y: stats.prise["2"] ? stats.prise["2"].avg : 0,
                }, {
                    name: "Garde Contre",
                    y: stats.prise["3"] ? stats.prise["3"].avg : 0,
                }]
            }]
        });
        const bouts = Highcharts.chart('bouts', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Repartition bouts'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: "0",
                    y: stats.bouts["0"] ? stats.bouts["0"].avg : 0,
                }, {
                    name: "1",
                    y: stats.bouts["1"] ? stats.bouts["1"].avg : 0,
                }, {
                    name: "2",
                    y: stats.bouts["2"] ? stats.bouts["2"].avg : 0,
                }, {
                    name: "3",
                    y: stats.bouts["3"] ? stats.bouts["3"].avg : 0,
                }]
            }]
        });
        const preneurs = Highcharts.chart('preneurs', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Repartition preneur'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: game.p1,
                    y: stats.preneur["0"] ? stats.preneur["0"].avg : 0,
                }, {
                    name: game.p2,
                    y: stats.preneur["1"] ? stats.preneur["1"].avg : 0,
                }, {
                    name: game.p3,
                    y: stats.preneur["2"] ? stats.preneur["2"].avg : 0,

                }]
            }]
        });
        const bar = Highcharts.chart('win_hand', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Mains gagnés'
            },
            subtitle: {
                text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
            },
            xAxis: {
                categories: [game.p1, game.p2, game.p3],
                title: {
                    text: null
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Population (millions)',
                    align: 'high'
                },
                labels: {
                    overflow: 'justify'
                }
            },
            tooltip: {
                valueSuffix: ' millions'
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 80,
                floating: true,
                borderWidth: 1,
                backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                shadow: true
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Win total',
                data: stats.win_hand_by_player
            }]
        });
    };

    function checkForm() {
        let players = {!! json_encode($game["players_number"]) !!};
        if (players < 5) {
            if ($('.input_checked').length < 3) {
                $("#error").html('<p>Touts les bouttons ne sont pas cochés</p> ');
                return false;
            }
        } else {
            if ($('.input_checked').length < 4) {
                $("#error").html('<p>Touts les bouttons ne sont pas cochés</p> ');
                return false;
            }
        }
    }

    function calcPoints(pointsNeeded, val) {
        let defense_points = 91 - val;
        let attack_win;
        attack_win = val > pointsNeeded;
        $('#attack_points').html(val);
        $('#defense_points').html(defense_points);
    }
    function pointsNeeded (e) {
        console.log(e)
        let points_needed = 56;
        switch (e) {
            case "0":
                points_needed = 56;
                break;
            case "1":
                points_needed = 51;
                break;
            case "2":
                points_needed = 41;
                break;
            case "3":
                points_needed = 36;
        }
        $("#points_needed").html(points_needed);
        if ($("#points_slider").val() > points_needed) {
            $("#indicator").css("color", "green");
        } else {
            $("#indicator").css("color", "red");
        }
        return points_needed;
    }

</script>

