<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @yield('css')
</head>
<body>
<div id="app">
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <nav>
        <div class="nav-wrapper">
            <div id="home">

                <div id="logo"></div>
                <a href="{{ url('/home') }}" class="brand-logo">

                    Compteur Tarot</a>
            </div>
            @guest
                <ul class="right hide-on-med-and-down">
                    <li><a href="{{ route('login') }}">{{ __('Login') }}</a></li>
                    <li><a href="{{ route('register') }}">{{ __('Register') }}</a></li>
                </ul>
            @else
                <ul class="right hide-on-med-and-down">
                    <li class="top-bar">
                        <ul>
                            <li><a href="{{ route('logout') }}" class="logout">{{ __('Logout') }}</a></li>
                        </ul>
                        <a materialize="dropdown" class='dropdown-button btn' href="#" data-activates="dropdown1">{{ Auth::user()->name }}<i
                                class="material-icons right">arrow_drop_down</i></a>
                        <ul id='dropdown1' class='dropdown-content'>
                            @if (Auth::user()->notif !== null)
                            @foreach(Auth::user()->notif as $not)
                                @if($not["value"] == 0)
                                        <li><a href={{route('notif',['_id' => $not['game'], 'sender'=>$not['sender']])}}>{{$not["sender"]}} invites you !</a></li>
                                    @endif
                            @endforeach

                            @endif
                        </ul>
                    </li>
                </ul>



            @endguest
        </div>
    </nav>
    @yield('content')
</div>
<!-- Scripts -->

<script src="{{ asset('js/app.js') }}">

</script>
<script>
    $( document ).ready(() => {
        $('.dropdown-button').dropdown();
    });
</script>

</body>
</html>
