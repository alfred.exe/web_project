@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="row">

                {!! Form::open(['route' => 'game.create', 'onsubmit'=>'return checkForm()']) !!}
                <div class="radio-input">
                    <div class="switch">
                        <label>
                            3 Joueurs
                            <input type="checkbox" class="radio p3" name="players" value="3" checked>
                            <span class="lever"></span>
                        </label>
                    </div>
                    <div class="switch">
                        <label>
                            4 Joueurs
                            <input type="checkbox" class="radio p4" name="players" value="4">
                            <span class="lever"></span>

                        </label>
                    </div>
                    <div class="switch">
                        <label>
                            5 Joueurs
                            <input type="checkbox" class="radio p5" name="players" value="5">
                            <span class="lever"></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field">
                        {!! Form::text('p1', Auth::user()->name, array('readonly' => 'readonly')) !!}
                        <label for="P1">P1</label>
                    </div>

                    <div class="input-field">
                        {!! Form::text('p2', null, ['class' => 'autocomplete',"id"=>"p2"]) !!}
                        <label for="P2">P2</label>

                    </div>
                    <div class="input-field">
                        {!! Form::text('p3', null, ['class' => 'autocomplete',"id"=>"p3"]) !!}
                        <label for="P3">P3</label>

                    </div>
                    <div class="input-field" id="p4_field">
                        {!! Form::text('p4', null, ['class' => 'autocomplete',"id"=>"p4"]) !!}
                        <label for="P3">P3</label>

                    </div>
                    <div class="input-field" id="p5_field">
                        {!! Form::text('p5', null, ['class' => 'autocomplete',"id"=>"p5"]) !!}
                        <label for="P3">P3</label>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </div>
                    {!! Form::submit('Submit', ['class' => 'btn waves-effect waves-light']) !!}
                    {!! Form::close() !!}
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    // the selector will match all input ctrols of type :checkbox
    // and attach a click event handler
    window.onload = function () {
        $('.p3').on('click load', function () {
            if ($(this).is(':checked')) {
                $("#p4_field").hide();
                $("#p5_field").hide();
            } else {
            }
        });
        $('.p4').on('click', function () {
            if ($(this).is(':checked')) {
                $("#p4_field").show();
                $("#p5_field").hide();
            } else {
                $("#p4_field").hide();
                $("#p5_field").hide();
            }
        });
        $('.p5').on('click', function () {
            if ($(this).is(':checked')) {
                $("#p4_field").show();
                $("#p5_field").show();
            } else {
                $("#p4_field").hide();
                $("#p5_field").hide();
            }
        });
        $("input:checkbox").on('click', function () {
            // in the handler, 'this' refers to the box clicked on
            var $box = $(this);
            if ($box.is(":checked")) {
                // the name of the box is retrieved using the .attr() method
                // as it is assumed and expected to be immutable
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                // the checked state of the group/box on the other hand will change
                // and the current value is retrieved using .prop() method
                $(group).prop("checked", false);
                $box.prop("checked", true);
            } else {
                $box.prop("checked", false);
            }
        });
        var route = "{{ url('autocomplete') }}";
        var auto_data;

        function auto(term) {
            $.get(route, {term: $(".autocomplete").val()}, function (data) {
                auto_data = data;
                $('.autocomplete').autocomplete({
                    data: data
                });
            })
        }

        const $p2 = $("#p2");
        const $p3 = $("#p3");
        const $p4 = $("#p4");
        const $p5 = $("#p5");
        $p2.on('input', function auto(term) {
            $.get(route, {term: $("#p2").val()}, function (data) {
                auto_data = data;
                console.log(data);
                $('#p2').autocomplete({
                    data: data
                });
            })
        });
        $p3.on('input', function (term) {
            $.get(route, {term: $p3.val()}, function (data) {
                auto_data = data;
                console.log(data);
                $p3.autocomplete({
                    data: data
                });
            })
        });
        $p4.on('input', function (term) {
            $.get(route, {term: $p4.val()}, function (data) {
                auto_data = data;
                console.log(data);
                $p4.autocomplete({
                    data: data
                });
            })
        });
        $p4.on('input', function (term) {
            $.get(route, {term: $p4.val()}, function (data) {
                auto_data = data;
                console.log(data);
                $p4.autocomplete({
                    data: data
                });
            })
        });
        $p5.on('input', function (term) {
            $.get(route, {term: $p5.val()}, function (data) {
                auto_data = data;
                console.log(data);
                $p5.autocomplete({
                    data: data
                });
            })
        });

        Echo.private('test')
            .listen('NewNotif', (e) => {
                console.log('Got event...');
            });
    };
    function checkForm() {
        let inputs = [
            $('input[name$="p1"]').val(),
            $('input[name$="p2"]').val(),
            $('input[name$="p3"]').val(),
        ]
        let p4 = $('input[name$="p4"]').val();
        if (p4) {
            inputs.push(p4).val()
        }
        let p5 = $('input[name$="p5"]').val();
        if (p5) {
            inputs.push(p5).val()
        }
        if (!(inputs.length === new Set(inputs).size)) {
            //TODO real error
            console.log("higiu");
            return false;
        }
    }


</script>

