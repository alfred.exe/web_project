<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Array_;

class CompteurController extends Controller
{
    static function points_needed(int $j)
    {
        $ret = 0;

        if ($j == 0) {
            $ret = 56;
        } else if ($j == 1) {
            $ret = 51;
        } else if ($j == 2) {
            $ret = 41;
        } else if ($j == 3) {
            $ret = 36;
        }
        return $ret;
    }

    static function coeff(int $prise)
    {
        $ret = 1;
        switch ($prise) {
            case 1:
                $ret = 2;
                break;
            case 2:
                $ret = 4;
                break;
            case 3:
                $ret = 6;
                break;
        }
        return $ret;

    }

    static function count($player_number, $score, $bouts, $prise, $preneur, $appele = null)
    {
        $ret = [];
        $diff_score = $score - self::points_needed($bouts);
        $coeff = self::coeff($prise);
        $score_brut = ($diff_score + 25) * $coeff;
        switch ($player_number) {
            case (3):
                {
                    switch ($preneur) {
                        case 0:
                            $ret [0] = $score_brut * 2;
                            $ret [1] = -$score_brut;
                            $ret [2] = -$score_brut;
                            break;
                        case 1:
                            $ret [0] = -$score_brut;
                            $ret [1] = $score_brut * 2;
                            $ret [2] = -$score_brut;
                            break;
                        case 2:
                            $ret [0] = -$score_brut;
                            $ret [1] = -$score_brut;
                            $ret [2] =  $score_brut * 2;
                            break;

                    }
                }
                break;

            case (4):
                {
                    switch ($preneur) {
                        case 0:
                            $ret [0] = $score_brut * 3;
                            $ret [1] = -$score_brut;
                            $ret [2] = -$score_brut;
                            $ret [3] = -$score_brut;
                            break;
                        case 1:
                            $ret [0] = -$score_brut;
                            $ret [1] = $score_brut * 3;
                            $ret [2] = -$score_brut;
                            $ret [3] = -$score_brut;
                            break;
                        case 2:
                            $ret [0] = -$score_brut;
                            $ret [1] = -$score_brut;
                            $ret [2] = $score_brut * 3;
                            $ret [3] = -$score_brut;
                            break;
                        case 3:
                            $ret [0] = -$score_brut;
                            $ret [1] = -$score_brut;
                            $ret [2] = -$score_brut;
                            $ret [3] = $score_brut * 3;
                            break;

                    }
                }
                break;

            case (5):
            {
                switch ($preneur) {
                    case 0:
                        $ret [0] = $score_brut * 2;
                        $ret [1] = -$score_brut;
                        $ret [2] = -$score_brut;
                        break;
                    case 1:
                        $ret [0] = -$score_brut;
                        $ret [1] = $score_brut * 2;
                        $ret [2] = -$score_brut;
                        break;
                    case 2:
                        $ret [0] = -$score_brut;
                        $ret [1] = -$score_brut;
                        $ret [2] = $score_brut * 2;
                        break;
                }
            }
        }
error_log(print_r($ret));
        return $ret;
    }
    //TODO annonces and 5 players
    /*private Integer[] annonces(String[] annonces) {

    }*/
}
