<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $_name = Auth::user()->name;
        $data = Game::where('p1', '=', $_name)
            ->orWhere('p2', "=", $_name)
            ->orWhere('p3', "=", $_name)
            ->orWhere('p4', "=", $_name)
            ->orWhere('p5', "=", $_name)
            ->orderBy('updated_at', 'desc')
            ->get();
        return view('home', ['data' => $data]);
    }
}
