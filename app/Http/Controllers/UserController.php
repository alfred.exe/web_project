<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private function push_notif()
    {

    }

    static public function add_notif($_id, $p1, $p2, $p3, $p4, $p5)

    {
        $contact = [];
        $contact['p2'] = $p2;
        $contact['p3'] = $p3;
        if ($p4 !== null) {
            $contact['p4'] = $p4;

        }
        if ($p5 !== null) {
            $contact['p5'] = $p5;

        }
        $notif = [];
        $notif["value"] = 0;
        $notif["sender"] = $p1;;
        $notif["game"] = $_id;;
        foreach ($contact as $player) {

            User::where(
                'name', '=',
                $player)->push('notif', $notif);
        }
    }

    static public function remove_notif($_id, $sender)
    {
        User::find(Auth::id())
            ->pull('notif', ["value" => 0,
            "sender" => $sender,
            "game" => $_id]);
        GameController::join_game($_id, $sender);
        return back();
    }
}
