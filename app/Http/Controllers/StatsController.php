<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StatsController extends Controller
{
    public static function generateGameStats($players_number, $hands, $inputs)
    {
        $line_chart = [[]];

        $max_value = 0;
        $index_max_hand = 0;
        foreach ($hands as $index => $hand) {
            if ($index == 0) {
                $line_chart[0][$index] = $hand[0];
                $line_chart[1][$index] = $hand[1];
                $line_chart[2][$index] = $hand[2];
                if ($players_number > 3) {
                    $line_chart[3][$index] = $hand[3];
                }
                if ($players_number > 4) {
                    $line_chart[4][$index] = $hand[4];
                }
            } else {

                $line_chart[0][$index] = $line_chart[0][$index-1] +$hand[0];
                $line_chart[1][$index] = $line_chart[1][$index-1] + $hand[1];
                $line_chart[2][$index] = $line_chart[2][$index-1] + $hand[2];
                if ($players_number > 3) {
                    $line_chart[3][$index] += $hand[3];
                }
                if ($players_number > 4) {
                    $line_chart[4][$index] += $hand[4];
                }
            }

            if (max($hand) > $max_value) {
                $index_max_hand = $index;
            }
        }

        $prise_array = [];

        $bouts_array = [];

        $preneur_array = [];

        $appele_array = [];

        $win_loose_hand_array = [];

        $win_hand_by_player = [0, 0, 0];
        if ($players_number > 3) {
            $win_hand_by_player[3] = 0;
        }
        if ($players_number > 4) {
            $win_hand_by_player[4] = 0;
        }

        //TO DO ajouter des stats sur les reussites en attaque et défense par joueur
        foreach ($inputs as $index => $input) {
            $bouts_array[$index] = $input[1];
            $prise_array[$index] = $input[2];
            $preneur_array[$index] = $input[3];
            $appele_array[$index] = $input[4];
            $points_needed = CompteurController::points_needed($input[1]);
            if ($input[0] > $points_needed) {
                $win_loose_hand_array[$index] = 1;
                $win_hand_by_player[$input[3]]++;
                //TO DO faire un truc moins con
            } else {
                $win_loose_hand_array[$index] = 0;
                foreach ($win_hand_by_player as $key=>$e) {
                    if ($key != $input[3]) {
                        $win_hand_by_player[$key]++;
                    }
                }
            }

        }


        $ret['line'] = $line_chart;
        $ret['bouts'] = self::array_avg($bouts_array);
        $ret['prise'] = self::array_avg($prise_array);
        $ret['preneur'] = self::array_avg($preneur_array);
        $ret['win_loose'] = self::array_avg($win_loose_hand_array);
        $ret['win_hand_by_player'] = $win_hand_by_player;
        $ret['index_max_hand'] = $index_max_hand;
        if ($players_number > 4) {

            $ret['appele'] = self::array_avg($appele_array);
        }
        return $ret;
    }

    static function array_avg($array, $round = 1)
    {
        $num = count($array);
        return array_map(
            function ($val) use ($num, $round) {
                return array('count' => $val, 'avg' => round($val / $num * 100, $round));
            },
            array_count_values($array));
    }

}
