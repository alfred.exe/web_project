<?php

namespace App\Http\Controllers;

use App\Events\NewNotif;
use App\Http\Controllers\CompteurController;
use Illuminate\Http\Request;
use App\Game;
use Illuminate\Support\Facades\Auth;


class GameController extends Controller
{
    public function create()
    {
        return view('create');
    }

    public function store(Request $request)

    {
        $validatedData = $request->validate([
            'p1' => 'required|exists:users,name',
            'p2' => 'required|exists:users,name',
            'p3' => 'required|exists:users,name',
            'p4' => 'nullable|exists:users,name',
            'p5' => 'nullable|exists:users,name',
        ]) ;

        $game = [];

        $game['p1'] = $request->get('p1');
        $game['p2'] = $request->get('p2');
        $game['p3'] = $request->get('p3');
        $game["wait"] = [];
        array_push($game["wait"],$request->get('p2'),$request->get('p3'));
        $game['players_number']=$request->get('players');
        if ($request->get('p4') !== null && $request->get('p5') !== "") {

            $game['wait']["p2"] = $request->get('p4');
            array_push($game["wait"],$request->get('p4'));
        }
        if ($request->get('p5') !== null && $request->get('p5') !== "") {
            $game['wait']["p5"] = $request->get('p5');
            array_push($game["wait"],$request->get('p5'));
        }
        $game["status"] = 0;
        $game = Game::create($game);
        error_log('game/'.$game->_id);
        return redirect()->route("game", ['_id'=>$game->_id]);
    }

    public function view($_id)
    {
        $game = Game::find($_id);
        $stats=null;
        if (isset($game->hand)) {
            if (count($game->hand) > 1) {
                $stats = StatsController::generateGameStats($game->players_number, $game->hand,$game->input);
            }
        }

        return view('game_page', ["game" => $game, "stats"=>$stats]);
    }

    public static function join_game ($game_id, $p_id) {
        Game::find($game_id)
            ->pull('wait', Auth::user()->name);
        $game = Game::find($game_id);
        error_log(print_r($game_id, true));
        error_log(print_r($game->wait, true));
        if (empty($game->wait)) {
            Game::find($game_id)->update(['status' => 1]);
        }
        return redirect()->route("game", ['_id'=>$game_id]);
    }

    public function getInput(Request $request)
    {
        $game = Game::find( $search = $request->get('game'));
        return response() -> json($game -> input[$request->get('index')]);
    }

    function generateParams(Request $req)
    {
        $hand = CompteurController::count(Game::find($req->_id)->players_number, $req->points, $req->bouts, $req->prise, $req->preneur, $req->appele);
        $input = [$req->points, $req->bouts, $req->prise, $req->preneur, $req->appele];
        //TO DO Try to use this variable
        $game = Game::find($req->_id);
        if (!empty($req->index)) {
            $old_hands = Game::find($req->_id)->hand;
            $old_hands[$req->index] = $hand;
            Game::find($req->_id)->update(['hand'=>$old_hands]);
            $old_input = Game::find($req->_id)->hand;
            $old_input[$req->index] = $input;
            Game::find($req->_id)->update(['hand'=>$old_hands]);
        }
        else {
            Game::find($req->_id)->push('hand', [$hand]);
            Game::find($req->_id)->push('input', [$input]);
        }
        return back();
    }




}
