<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    //
    public function search(Request $request)
    {
        $search = $request->get('term');

        $result = User::where('name', 'LIKE', '%'. $search. '%')->get();
        $ret = [];
        foreach($result as $item) {
            $ret[$item["name"]]=null;
        }

        return response()->json($ret);

    }
}
