<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GameFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'p1' => 'required|exists:user,name',
            'p2' => 'required|exists:user,name',
            'p3' => 'required|exists:user,name',
            'p4' => 'exists:user,name',
            'p5' => 'exists:user,name'
        ];
    }
}
