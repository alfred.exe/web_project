<?php

namespace App\Providers;

use App\Game;
use Illuminate\Support\ServiceProvider;

class GameControllerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Http\Controllers\GameController', function () {
            return new GameController();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
