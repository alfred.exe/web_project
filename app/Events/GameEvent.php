<?php

namespace App\Events;

use App\Game;
use Illuminate\Queue\SerializesModels;

class GameEvent
{
    use SerializesModels;

    public $game;

    /**
     * Create a new event instance.
     *
     * @param \App\Game $game
     */
    public function __construct(Game $game)
    {
        $this->game = $game;
    }
}
