<?php

namespace App;

use App\Events\GameEvent;
use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model;

class Game extends Model
{
    protected $collection = 'games';
    /**
     * @var int
     */
    private $status;
    private $p1;
    private $p2;
    private $p3;
    private $hands;
    protected $guarded = ['id'];



 /*   public static function addHand($_id, $hand)
    {
        $games = Game::find;;

    }*/

    function get()
    {
        $games = Game::all();
        $ret = array();
        foreach ($games as $key => $value) {

            $ret[$key] = json_decode($value, true);
        }
        return $ret;
    }
    use Notifiable;

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => GameEvent::class,
    ];

}
