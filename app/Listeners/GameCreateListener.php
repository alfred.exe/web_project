<?php

namespace App\Listeners;

use App\Events\GameEvent;
use App\Http\Controllers\GameController;
use App\Http\Controllers\UserController;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GameCreateListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(GameEvent $event)
    {
       UserController::add_notif($event->game->_id,$event->game->p1,$event->game->p2,$event->game->p3,$event->game->p4,$event->game->p5);
    }
}


